﻿#include <iostream>

int main()
{
    std::string word;
    std::cout << "Input word: ";
    std::cin >> word;

    std::cout << "Word: " << word << " " << "Length: " << word.length() << std::endl;
    std::cout << "First char: " << word[0] << " " << "Last char: " << word[word.length() - 1];
}